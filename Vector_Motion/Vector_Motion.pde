Mover mover;
void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1,0);
  
}

void draw()
{
  background(255);

  
  mover.render();
  mover.update();
  mover.setColor(155, 25, 200, 255);
  
  if (mover.position.x > Window.right)mover.position.x = Window.left;
  if (mover.position.x < Window.left)mover.position.x = Window.right;
  
  //decelerates when it reaches center
  if (mover.position.x >= 0) mover.acceleration = new PVector(-0.15,0);
  
  //to make it break completelely
  if(mover.velocity.x <= 0)
  {
    mover.velocity.x = 0;
    mover.acceleration = new PVector(0,0);
  }
 
}
