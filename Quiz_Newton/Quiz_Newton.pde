Mover mover;
Mover[] movers = new Mover[10];
int timePassed = 0;
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.mass = 10;
  
  for (int i = 0; i < 10; i++)
  {
    movers[i] = new Mover(Window.left + 50, 250);
    movers[i].mass = i + 1;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.12f, 0);


void draw()
{
  background(255);
  
  noStroke();
  for (Mover m : movers)
  {
    m.applyGravity();
    m.applyFriction();
    
    m.applyForce(wind);
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
  }
  if(timePassed >= 2300) // to refresh everything when they stop
  {
    timePassed=0;
    setup();
  }
  timePassed++;
  

}
