void setup()
{
  size(1920, 1080, P3D);
  background(255);
  camera(0,0, -(height/2)/tan(PI*30/180), //camera position
        0,0,0, // eye position
        0, -1, 0); // up vector
}

int frameResetCount = 0;

void draw()
{
  
  float meanX = 0;
  float stdX = 300;
  
  float meanSize = 30;
  float stdSize = 20;
  
  float gaussX = randomGaussian();
  float gaussSize = randomGaussian();
  
  float x = stdX* gaussX + meanX;
  float y = random(-500, 501);
  float circleSize = stdSize * gaussSize + meanSize;
  
  noStroke();
  fill(random(255), random(255), random(255), random(10,16));
  circle(x,y,circleSize);
  
  frameResetCount++;
  
  if(frameResetCount == 1000)
  {
    background (255);
    frameResetCount = 0;
  }
}
