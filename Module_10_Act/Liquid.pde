class Liquid
{
   public float x, y;
   public float horizon, depth;
   public float cd;
   
   public Liquid(float x, float y, float horizon, float depth, float cd)
   {
     this.x = x;
     this.y = y;
     this.horizon = horizon;
     this.depth = depth;
     this.cd = cd;
   }
   
   public void render()
   {
     fill(28, 120, 186);
     beginShape();
     vertex(x - horizon, y, 0);
     vertex(x + horizon, y, 0);
     vertex(x + horizon, y + depth, 0);
     vertex(x - horizon, y + depth, 0);
     endShape();
   }
   
   public boolean isCollidingWith(Mover mover)
   {
      PVector pos = mover.position;
      return pos.x > this.x - this.horizon &&
          pos.x < this.x + this.horizon &&
          pos.y < this.y;
   }
   
   public PVector calculateDragForce(Mover mover)
   {
      // magnitude is coefficient * speed squared
      float speed = mover.velocity.mag();
      float dragMagnitude = this.cd * speed * speed;
      
      
      // direction is inverse of velocity
      PVector dragForce = mover.velocity.copy();
      dragForce.mult(-1);
      
      // scale according to magnitude
      dragForce.normalize();
      dragForce.mult(dragMagnitude);
      return dragForce;
   }
}
