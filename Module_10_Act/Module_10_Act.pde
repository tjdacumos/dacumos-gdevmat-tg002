Mover mover;
Mover[] movers = new Mover[10];
int timePassed = 0;
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);


void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < 10; i++)
  { 
    movers[i] = new Mover(Window.left + 80+(98*i), Window.top);
    movers[i].mass = 11-i;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
  
}

PVector wind = new PVector(0.2f, 0);

void draw()
{
  background(255);
  
  ocean.render();
  noStroke();
  for (Mover m: movers)
  {
    m.applyGravity();
    m.applyFriction();

    if(m.position.y >= -100) m.applyForce(wind);
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(m))
    {
      m.applyForce(ocean.calculateDragForce(m)); 
    }
  }
  if(timePassed >= 300) // to refresh everything when they stop
  {
    timePassed=0;
    setup();
  }
  timePassed++;

}
