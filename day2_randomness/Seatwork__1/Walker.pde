class Walker
{
 float xPosition; //by default it's always public
 float yPosition;
 
 void render()
 {
   circle(xPosition, yPosition, 30);
   fill(random(255), random(255), random(255), random(255));
   noStroke();
 }
 
 void randomWalk()
 {
   int decision = floor(random(8));
   
   if(decision == 0)
   {
     yPosition+=5;
   }
   else if (decision == 1)
   {
     yPosition-=5;
   }
   else if(decision == 2)
   {
     xPosition+=5;
   }
   else if(decision ==3)
   {
     xPosition-=5;
   }
   else if(decision == 4)
   {
     xPosition+=5;
     yPosition+=5;
   }
   else if(decision == 5)
   {
     xPosition+=5;
     yPosition-=5;
   }
   else if(decision ==6)
   {
     xPosition-=5;
     yPosition+=5;
   }
   else if (decision == 7)
   {
     xPosition-=5;
     yPosition-=5;
   }
 }
}
