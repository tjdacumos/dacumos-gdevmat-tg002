void setup()
{
  size(1920, 1080, P3D);
  
  camera(0,0, -(height/2)/tan(PI*30/180), //camera position
        0,0,0, // eye position
        0, -1, 0); // up vector
}

Walker walker = new Walker();

void draw()
{
  walker.render();
  walker.randomWalk();
}
