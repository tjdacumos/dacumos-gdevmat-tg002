void setup()
{
  size(1920, 1080, P3D);
  
  camera(0,0, -(height/2)/tan(PI*30/180), //camera position
        0,0,0, // eye position
        0, -1, 0); // up vector
}

Walker walker = new Walker();
Vector2 velocity = new Vector2(5,8);

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x,y);
}
void draw()
{
  background(0);
  Vector2 mouse = mousePos();
  
  mouse.normalize();
 
 //light emitted by the saber
  mouse.mult(500);
  strokeWeight(12);
  strokeCap(ROUND);
  stroke(125,0,0);
  line(0,0, mouse.x, mouse.y);
  line(0,0, -mouse.x, -mouse.y);
  
  //lightsaber itself
  strokeWeight(4);
  stroke(255,0,0);
  line(0,0, mouse.x, mouse.y);
  line(0,0, -mouse.x, -mouse.y);
  
  //handle
  mouse.div(10);
  strokeWeight(15);
  stroke(200,200,200);
  line(0,0, mouse.x, mouse.y);
  line(0,0, -mouse.x, -mouse.y);
  
}
