class Walker
{
 float xPosition; //by default it's always public
 float yPosition;
 float xT = -50; // time for pos X
 float yT = 50; // time for pos Y
 float rT = 200; // red time
 float gT = 100; // green time
 float bT = 150;// blue time 
 float sizeT = 30; //time for size
 
 void render()
 {  
   // to make the color of the circle better I removed the transparency (4th argument)
   fill(map(noise(rT),0,1,0,255), map(noise(gT),0,1,0,255), map(noise(bT),0,1,0,255));
   noStroke();
   
   circle(xPosition, yPosition, map(noise(sizeT), 0, 1, 30, 70)); 
   
   rT+=0.1;
   gT+=0.1;
   bT+=0.1;
   sizeT+=0.1;  
 }
 
 void noiseWalk()
 {
   
   float posX = noise(xT);
   float posY = noise(yT);
   
   xPosition = floor(map(posX, 0,1 , -950, 951));
   yPosition = floor(map(posY, 0,1 , -450, 451));
   
   xT+=.01; // rate of change of position x
   yT+=.01; // rate of change of position y
   
 }
}
