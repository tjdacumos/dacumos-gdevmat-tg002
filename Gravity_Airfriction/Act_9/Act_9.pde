Mover mover;
Mover[] movers = new Mover[5];
int timePassed = 0;
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < 5; i++)
  { 
    float r = random(1,10);
    movers[i] = new Mover(Window.left + 50, 300-(150*i));
    movers[i].mass = r;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }

}

PVector wind = new PVector(0.07f, 0);


void draw()
{
  background(255);
  
  noStroke();
  for (Mover m : movers)
  {
    if (m.position.x < 0) m.applyForce(wind);
    if (m.position.x >= 0) m.applyFriction();
    m.render();
    m.update(); 
  }
  if(timePassed >= 700) // to refresh everything when they stop
  {
    timePassed=0;
    setup();
  }
  timePassed++;

}
