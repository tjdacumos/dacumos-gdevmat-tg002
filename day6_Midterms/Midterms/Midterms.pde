void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  initializeObjects();

}

//times
float blT = random(255); // alpha time
float speedT = random(15);// speed time
float qT = random(100); // quantity time (just for fun)

//quantity of arrays or numbers of other matters
int quantity = 100;

//timer for when everything will reset
int timeFrame = 105;
int timePassed = 0;
int speed;

Mover blackhole;
Mover[] matters = new Mover[quantity];

//setting the starting positions of each matter(object) 
void initializeObjects()
{
   quantity = floor(map(noise(qT),0,1,100,200)); // just for fun
   qT++;
   blackhole = new Mover((random(Window.left, Window.right)), (random(Window.bottom, Window.top)),50);
  for(int i=0; i <matters.length; i++)
   {
    
   //matters position
   float stdX = 100;
   float stdY = 100;
   
   float gaussX = randomGaussian();
   float gaussY = randomGaussian();
   
   float meanX = random(Window.left, Window.right);
   float meanY = random(Window.bottom, Window.top);
   
   float x = stdX* gaussX + meanX;
   float y = stdY* gaussY + meanY;
   
   //matter quantity
   float scale = random(15,40);
   
   matters[i] = new Mover(x,y, scale);
  
   //color of matters
   float red = map(noise(random(200)),0,1,0,255); //perlin just for the sake of it
   float green = map(noise(random(200)),0,1,0,255); 
   float blue =  map(noise(random(200)),0,1,0,255);
   float black = map(noise(blT),0,1, 0, 255);
   
   matters[i].setColor(red,green,blue,black);
   }
   speed = floor(map(noise(speedT),0,1, 10,25));
 
}

void draw()
{
  background(0);
  
  if (timePassed < timeFrame) 
  {
    noStroke();
    
    for(int i=0; i<matters.length; i++)
   {
     matters[i].render();
     PVector direction = PVector.sub(blackhole.position, matters[i].position);
     direction.normalize(); // the one line that I needed that took me 5 hours to realise
     direction.mult(speed);
     matters[i].position.add(direction);
   }
   //placement important to maintain in on top
   blackhole.render();
   blT++;
  }
  else if(timePassed >= timeFrame) // resets everything
  {
     initializeObjects();
     timePassed = 0;
  } 
  timePassed++;
}
